/**
 * Represents the Background
 * @constructor
 */
function Background() {
	/**
	 *
	 * @type {number} poxX - Represents the current location of the Background
	 */
	var posX = 0;
	/**
	 * Represents the movement of the Background -
	 * @constructor
	 */
	this.draw = function () {
		/**
		 *
		 * @type {number} posX = (posX-1) % width Represents the movement into left
		 */
		posX = (posX - 1) % width;
		/**
		 * Represents the Background Positon
		 * @image
		 */
		image(images.background, posX, 0);
	}
}
