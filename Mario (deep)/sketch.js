'use strict';

var images;
var mario;
var bg;
var obstacles = [];
var GROUNDLEVEL = 360;
function preload() {
	var imgBackground = loadImage("assets/newBackground.jpg");
	var imgMario = loadImage("assets/mario.jpg");
	var imgKnochenbrowser = loadImage("assets/Knochenbowser.jpg");
	var imgDonkeyKong = loadImage("assets/DonkeyKong.jpg");
	var imgPiranhaPlanze = loadImage("assets/PiranhaPflanze.jpg");
    images = {
		background: imgBackground,
		mario: imgMario,
		bowser: imgKnochenbrowser,
		donkeykong: imgDonkeyKong,
		Pflanze: imgPiranhaPlanze

	};
    console.log("end of preload");
}
function setup() {
    createCanvas(800, 500);
    mario = new Mario();
	bg = new Background();
	for(var i=0; i<5; i++)
		obstacles.push(new Obstacle());
    console.log("end of setup");
}

function draw() {
	bg.draw();
	for(var i=0; i<obstacles.length; i++){
		obstacles[i].draw();
		if (obstacles[i].box.isCollission(mario.box)) {
			console.log("collission: " + mario.box.toString() + ", " + obstacles[i].box.toString());
			noLoop();
		}
	}
    mario.draw();
}

function keyPressed() {
  mario.jump();
}

function mousePressed() {
  loop();
}