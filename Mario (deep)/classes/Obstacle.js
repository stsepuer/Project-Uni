'use strict';

function Obstacle(){
	this.box = new Box(random(width, 2*width), GROUNDLEVEL, images.Pflanze.width, images.Pflanze.height);
	this.draw = function () {
		this.box.x = this.box.x - 1;
		if (this.box.x < 0) 
			this.box.x = random(width, 2*width);
		image(images.Pflanze, this.box.left(), this.box.top());

	}
}