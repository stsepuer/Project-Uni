
function Mario() {

	var jump = new Audio('jump.wav');
	var jumpHeight = 200;
	var size = 50;
	this.box = new Box(50, GROUNDLEVEL, size, size);
	var velocityY = 0;
	this.jump = function () {
		console.log("jump");
		jump.play();
		velocityY = -2;
	}


	this.draw = function () {
		this.box.y = this.box.y + velocityY;
		if (this.box.y < jumpHeight)
			velocityY = 4;
		if (this.box.y >= GROUNDLEVEL)
			velocityY = 0;
		image(images.mario, this.box.left(), this.box.top(), size, size);

		 // In Abhängigkeit des Tasten Druckes verändert sich das Bild
		if(key=='1'){console.log("Sie haben Mario ausgewählt.");image(images.mario, this.box.left(), this.box.top(), size, size);}
		else if(key=='2'){console.log("Sie haben den Knochenbowser ausgewählt.");image(images.bowser, this.box.left(), this.box.top(), size, size);}
		else if(key=='3'){console.log("Sie haben DoneyKong ausgewählt.");image(images.donkeykong, this.box.left(), this.box.top(), size, size);}


	}
}